#variables

x = 100     #int object with value of 100
            #if we asign a new value to x, for example 500,
            #what will happen here is that the python will create another
            #object with the value 500 and will point it to the X

a = 496
print(id(a))        # id function returns an unique identifier for an object
b = 54
print(id(b))
a=b
print(a)
print(b)
print(id(a)==id(b)) #The output tells us if the id of a is equal to the id
                    # of b, that means they have the same identifier so it
                    #is the same object

#Also, insted of doing "id(a)==id(b)" we can use the "is" operator
print("Another operation:")
print(a is b)