separator = '*'



def banner(message, border='-'):
    line = border * len(message)
    print(line)
    print(message)
    print(line)


banner("Norwegian Blue")
banner("Follow your Heart","*")
banner("This is awesome!","@")


def add_span(menu=[]):
    menu.append("spam")
    return menu


def add_span_corected(menu = None):
    if menu is None:
        menu = []
    menu.append('spam')
    return menu


brakfast = ['bacon','ham']
print(brakfast)
print(add_span(brakfast))
lunch = ['baked beans']
add_span(lunch)
print(lunch)


print(separator*30)
print(add_span())
print(add_span())
print(add_span())
print(add_span())   #to corrent this problem, in the add_span definition we
                    # should add none as parameter and check if the menu is none,
                    #if it's none  then we shoul initialize with [],
print(separator*30)
print(add_span_corected())
print(add_span_corected())
print(add_span_corected())
print(add_span_corected())
print(separator*30)
