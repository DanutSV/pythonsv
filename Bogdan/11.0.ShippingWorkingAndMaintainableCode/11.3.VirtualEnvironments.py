#Virtual enviroment is a light-weight, self-contained python installation
# that the user can install id without administrator privileges
#
# To verify if you have the tool installed type in terminal :
#       "pyvenv"
#
# Another tool that we can use is:
#       "virtualenv"
#
# To install these tools you can run:
#   - sudo apt install pyvenv -> for installing pyvenv
#   - pip install virtualenv
#
#
# In this example I used pyvenv
#   pyvenv 11.0.ShippingWorkingAndMaintainableCode
#   source 11.0.ShippingWorkingAndMaintainableCode/activate
#




