import time
h = 33
i=0
if True:
    print("This should be printed")

if False:
    print("Don't expect that I will be printed")

if "isRaining":
    print("Hurraaay!")

if h > 34:
    print("I am grater than 34")
else:
    print("I am smaller than 34")

while i <= 9:
    print("This is a while loop with iterator value of: "+str(i))
    i = i + 2

# print("Give me a number (just a small home): ")
testVariable = 5
while testVariable:
    print("It's the final countdown: "+str(testVariable))
    testVariable = testVariable - 1
    time.sleep(1)
print("Game Over")