#defining some arguments to work with
from math import exp
a = 3
b = 4
c = 2.2

d = a + b
e = b - c

f = d*a
g = d/a
h = a**a
i = a ** c
print("**************************")
print("A = "+str(a)+"\tB = "+str(b)+"\tC = "+str(c))
print("**************************")
print("A + B = "+str(d))
print("B - C = "+str(e))
print("D * A = "+str(f))
print("D / A = "+str(g))
print("**************************")
print("A squared = "+str(h))
print("A to the power of C = "+str(i))
print("**************************")

d