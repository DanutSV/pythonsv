from math import acos
from math import acosh
#Initialize constants
radians = 0.45
radians2 = 10

print("***********************")
print("Testing math Functions")
print("***********************")


print("This math module is always available.  "
      "It provides access to the mathematical functions "
      "defined by the C standard.")
arcCosAnswer = acos(radians)

print("Arc cosine (measured in radians) of \"" + str(radians)
      + "\" is: " + str(arcCosAnswer))
print("***********************")

hyperbolicArcCos = acosh(radians2)
print("The inverse hyperbolic cosine of \""+str(radians2)+ "\" is: "
      +str(hyperbolicArcCos))
print("***********************")

print("")
print("")
