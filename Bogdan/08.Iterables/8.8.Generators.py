#Generators in Python

# Specify iterable sequences
#   - all generators are interators

# Are lazily evaluated
#   - the next value in the sequence is computed on demand

# Can model infinite squences
#   - such as data streams with no definite end

# Are composable into pipelines
#   - for natural stream processing


def gen123():
    yield 1
    yield 2
    yield 3

g = gen123()

# print(g)
print(next(g))
print(next(g))
print(next(g))


for v in gen123():
    print(v)




