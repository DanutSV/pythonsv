def lucas():
    yield 2
    a = 2
    b = 1
    while True:
        yield b
        a,b = b, a+b

for x in lucas():           #This will shortly print very large numbers
    print(x)                # Ctrl+F2 to STOP

