#Iteration protocols

#Iterable protocol
#   - Iterable object can be passed to the built-in iter() function to get
# an iterator iterator = iter(iterable)
#
#Iterator protocol
#   - Iterator objects can be passed to the built-in next() function to
# fetch the next item. item = next(iterator)

iterable = ['Spring', 'Summer', 'Autum', 'Winter']

iterator = iter(iterable)
print(next(iterator))
print(next(iterator))
print(next(iterator))
print(next(iterator))
#print(next(iterator))      # calling this fifth time would raise an exception

#We could define a function that would iterate the list for us

def first(iterable):
    iterator = iter(iterable)
    try:
        return next(iterator)
    except StopIteration:
        raise ValueError("Iterable is empty.")

first(iterable)

