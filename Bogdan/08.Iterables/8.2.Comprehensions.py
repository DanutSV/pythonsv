# Types of comprehensions:
#   - list comprehensions
#   - set comprehensions
#   - dictionary comprehensions
# with a style that can be :
#   - declarative
#   - functional
#   - readable
#   - expressive
#   - effective
from math import factorial


words = "Why sometimes I have believed as many as six impossible things " \
        "before breakfast".split()
print(words)

for word in words:
    print(len(word))


lengths = []

for word in words:
    lengths.append(len(word))
print(lengths)


f = [len(str(factorial(x))) for x in range(20)]
print(f)

char = '*'
print(char*50)
list = range(0,20)
for x in list:
    y = factorial(x)
    z = len(str(y))
    print("X = {}\t".format(x) + "length(x) = {}\t".format(z) +
          "factorial(X) = {}\t".format(y))