# Never catch this kind of errors:
#       IndentationError
#       SyntaxError
#       NameError
#
# This kind of errors are produced by programmers and they should never be
# there in first place

# Multiple exceptions can be cached like this:
import sys


def convert(s):
    try:
        return int(s)
    except(ValueError,TypeError,EnvironmentError,IOError,
           FloatingPointError) as e:
        print("Conversion error:{}".format(str(e)),file=sys.stderr)
        return -1


convert("da")

