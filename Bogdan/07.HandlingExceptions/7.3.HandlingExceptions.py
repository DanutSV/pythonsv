def convert(s):
    '''Convert to an integer'''
    try:
        x = int(s)
        print("Conversion succeeded! x =", x)
    except ValueError:
        print("Conversion failed!")
        x = -1
    except TypeError:
        print("Conversion failed, cannot accept this type!")
        x = -1
    return x


convert("This should brake")
convert(4)
convert([-3,4,10])
# Expected output:
#   Conversion failed!
#
