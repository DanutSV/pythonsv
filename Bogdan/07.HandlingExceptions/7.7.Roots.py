def sqrt(x):
    """
    Compute square roots using the method of Heron of Alexandria

    Args:
        x: The number for which the square root is to be computed/

    Returns:
        The square root of x.
    """
    if x < 0:
        raise ValueError("Invalid negative number: {}".format(x))
    guess = x
    i = 0
    while guess * guess != x and i < 20:
        guess = (guess + x / guess) / 2.0
        i += 1
    return guess

def main():
    print(sqrt(128))
    print(sqrt(64))
    print(sqrt(32))
    print(sqrt(16))
    print(sqrt(8))
    print(sqrt(4))
    print(sqrt(2))
    print(sqrt(-1))

if __name__ == '__main__':
    main()