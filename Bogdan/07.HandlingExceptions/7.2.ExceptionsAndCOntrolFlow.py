def convert(s):
    '''Convert to an integer'''
    x = int(s)
    return x


convert("This should brake")


# Expected output
#Traceback (most recent call last):
#  File "/home/bogdan.crisan/Documents/04.Git/Projects/pythonsv/Bogdan/07
# .HandlingExceptions/7.2.ExceptionsAndCOntrolFlow.py", line 7, in <module>
#    convert("This should brake")
#  File "/home/bogdan.crisan/Documents/04.Git/Projects/pythonsv/Bogdan/07
# .HandlingExceptions/7.2.ExceptionsAndCOntrolFlow.py", line 3, in convert
#    x = int(s)
# ValueError: invalid literal for int() with base 10: 'This should brake'

