from urllib.request import urlopen


def fetch_words():
    with urlopen(
            'http://www.yahoo.com') as story:
        story_words = []
        for line in story:
            line_words = line.decode('utf-8').split()
            for word in line_words:
                story_words.append(word)
    for word in story_words:
        print(word)
        # print("done")


if __name__ == '__main__':  # because of this if, you can import this
    # script as a module or you can run it from terminal
    fetch_words()
