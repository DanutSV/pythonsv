#Setting up a main() function with a command line argument
import sys
from urllib.request import urlopen


def fetch_words(url):
    with urlopen(url) as story:
        story_words  = []
        for line in story:
            line_words = line.decode('utf-8').split()
            for word in line_words:
                story_words.append(word)
    return story_words
                        #Use two line between functions
                        #This is what PEP 8 style guide tell to do
def print_items(items):
    for item in items:
        print(item)
        # print("done")


def main(url):
    words = fetch_words(url)
    print_items(words)


if __name__ == '__main__':  # because of this if, you can import this
                            # script as a module or you can run it from terminal
    main(sys.argv[1])
