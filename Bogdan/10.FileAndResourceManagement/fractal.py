import math


def mandel(real, imag):
    x = 0
    y = 0
    for i in range(1, 257):
        if x * x + y * y > 4:
            break
        xt = real + x * x - y * y
        x = xt
    return int(math.log(i) * 256 / math.log(256)) - 1


def mandelbrot(size_x, size_y):
    return [[mandel((3.5 * x / size_x) - 2.5,
                    (2.0 * y / size_y) - 1.5)
             for x in range(size_x)]
            for y in range(size_y)
            ]
