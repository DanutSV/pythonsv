h = open('something.txt', mode='at', encoding='utf-8')
h.writelines([
    '\nThis is a simple text\n'
    'That does not end on a single line\n',
    'And it was appended using at mode\n',
    'With standard encoding\n'
])
h.close()