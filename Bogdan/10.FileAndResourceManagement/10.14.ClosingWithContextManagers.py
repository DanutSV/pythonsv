from contextlib import closing

class RefrigeratorRaider:

    def open(self):
        print("Open fridge door.")

    def take(self, food):
        print("Finding {}...".format(food))
        if food == "deep fried pizza":
            raise RuntimeError("Health waring")
        print("Taking {}".format(food))

    def close(self):
        print("Close fridge door.")
        print(len("Close fridge door.") * '*')

def raid(food):
    with closing(RefrigeratorRaider()) as r:
        r.open()
        r.take(food)

if __name__ == '__main__':
    raid("bacon")
    raid("deep fried pizza")

