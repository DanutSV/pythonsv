def words_per_line(flo):
    return [len(line.split()) for line in flo.readlines()]

with open('something.txt', mode='rt', encoding='utf-8') as real_file:
    wpl = words_per_line(real_file)

print(wpl)

# we can also do the same for http requests

