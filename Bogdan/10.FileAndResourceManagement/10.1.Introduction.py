# Open a file:
#   file : path to file (required)
#   mode : read/write/append, binary/text
#   encoding : text encoding
#
# When the files are accessed they are either binary without any encoding or
# text
import sys
print(sys.getdefaultencoding())
print("Visit http:docs.python.org/3/library/codecs.html, #standard-encodings")


