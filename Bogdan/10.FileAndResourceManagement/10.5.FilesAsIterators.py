import sys
# As iterator, we use the lines of the something.txt file
# and with a simple for we can print the content of it
# but if you run the program you will observe something odd,
# there is a space between sentences, thus resulting in a problem
# because in the file only one new line exists
#
# The problem comes from the print function, because it has buit in
# carriage return, and with a new line, we get a bonus line
#
# To fix it, we need to use the system stdout.write method
#
g = open('something.txt',mode='rt', encoding='utf-8')
#
# for line in g:
#     print(line)
#
# Fix

for line in g:
    sys.stdout.write(line)

g.close()