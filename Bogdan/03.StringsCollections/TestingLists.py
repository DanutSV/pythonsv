#Pythons lists are mutable squences of objects

#E.G
myList1 = [1,3,5]
myList2 = ["Red", "Blue", "Yellow"]
for i in myList2:
    print("The color is: "+i)

print("Lists can be modified with append to add objects to a list")

print(myList2)
newElement = "Violet"
myList2.append(newElement)
print("Added a new element:"+newElement)
print(myList2)

print("Other things you can do to a list")

print(myList2.count("Red"))
myList2.reverse()
myList2.sort()
print(myList2)

print(myList2.clear())  #This should be null