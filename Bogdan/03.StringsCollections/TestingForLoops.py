agenda = {'Person1':'+407 XXXX XX XX',
          'Person2':'+407 XXXY XX XX',
          'Person3':'+407 XXYX XX XX',
          'Person4':'+407 XXYY XX XX', }


#Let's take the dictionary used before, and print it
print(agenda)

#The output looks something like ""/usr/bin/python3.5 /home/bogdan.crisan/Documents/04.Git/Projects/pythonsv/Bogdan/03.StringsCollections/TestingForLoops.py
#{'Person1': '+407 XXXX XX XX', 'Person4': '+407 XXYY XX XX', 'Person3':
# '+407 XXYX XX XX', 'Person2': '+407 XXXY XX XX'}
#""

#That is not useful
#Using for loop whould be more usefull

#E.G
print("\n")
for persons in agenda:
    print(persons,agenda[persons])
