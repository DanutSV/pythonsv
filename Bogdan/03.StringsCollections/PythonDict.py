# Dictionaries are mutable mappings of keys to values
# {k1: v1, k2: v2}
#   where   k1, k2 are the keys
#           v1, v2 are the values
# To access a value from a the dictionary you need to call the dictionary
# and give it the key that coresponds to the value you want to retrive

# For example

agenda = {'Person1':'+407 XXXX XX XX',
          'Person2':'+407 XXXY XX XX',
          'Person3':'+407 XXYX XX XX',
          'Person4':'+407 XXYY XX XX', }

print(agenda['Person3']) 