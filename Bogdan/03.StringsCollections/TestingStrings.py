#Strings
#
#To concatenate a string, yuu don't need a special function or
#something simmilar to do it

#E.G
print("First""Second")
print("\n") #This line is used to separate the printed output
#Between "First" and "Second" in print() function there is no
#separator, thus the printed output should be FirstSecond

#String and new lines

#To print on multiline you have a couple of options:
#   a. Printing with a single call of the print function that will contain an
#  escape character "\n"
#   b. Printing calling the print function multiple times

print("This is an example for case a \nThis should appear on the seccond row")
print("\n") #This line is used to separate the printed output
print("And this is for case b")
print("Where to print this line I used another print")
print("\n") #This line is used to separate the printed output

#   c.
justForPrint = "Let's say that we need to use this string multiple times in " \
               "our program\nAnd we want to have a new line from time to " \
               "time\nWe can do that by using the '\\n'\ntyped in the string"
print(justForPrint)
print("\n") #This line is used to separate the printed output

#Or if we want to use it in the print function we can use the triple """ to
# do it
print(""""
    This is a multiline text
    This is the second row
    3rd row
    bla bla
    Also, the tab we can insert it by using '\\t' or in this case hardcoded 
    in the text, 
""")
print("\n")

#Raw Strings, best used to store path to folders and so on
#E.G.
path = r'C:Users\TestUser\Documents\Spells'
print(path)


