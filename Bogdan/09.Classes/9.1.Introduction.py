# When the standard builtin types are not enough you can use classes to
# create custom types
#

print(type(5))
print(type("python"))
print(type(x * x for x in [2, 4, 6]))

# Classes define the structure and behavior of objects
# An object's class controls its initialization

# Classes are key feature for OOP (Object-Oriented Programming)
# Classes make complex problems tractable
# Classes can make simple solutions overly complex
# The fact that the language uses objects everywhere makes you decide when
# to use classes NOT forcing you to deal with it for simple solutions where
# the code must be very small, simple and efficient
# This sets the language apart from Java and C#




