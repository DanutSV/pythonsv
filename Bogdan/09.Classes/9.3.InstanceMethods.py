"""Model for aircraft flights."""
# Method: Functions defined with a class
# Instance method: functions which can be called on objects
# self the first argument to all instance methods


class Flight:
    def number(self):
        return "SN060"


f = Flight()
print(f.number())

# Or

print(Flight.number(f))     # It works but it is not very used

