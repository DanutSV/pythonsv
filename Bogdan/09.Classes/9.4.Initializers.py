# _number?
# 1. To avoid name clash with number()
# 2.By convention, implementation details start with underscore '_'


class Flight:
    def __init__(self, number):     # __init__ -> initializer
        if not number[:2].isaplpha():
            raise ValueError("No airline code in '{}'".format(number))
        if not number[:2].isupper():
            raise ValueError("Invalid airline code '{}'".format(number))
        if not (number[2:].isdigit() and int(number[2:]) <= 9999):
            raise ValueError("Invalid route number '{}'".format(number))
        self._number = number

    def number(self):
        return "SN060"
    def airline(self):
        return self._number[:2]
f = Flight("SN060")
print(f.number())
print(f._number)

g = Flight("060")
print(g.number())
print(g._number)
