# str - homogeneous immutable sequence of unicode codepoints or characters
import math

testString1 = "1. This is a test string "
testString2 = "2. This is another text that \nI want to be used to " \
              "concatenate " \
              "with first text string!"
separator = '*'

# find the length of the string
print(len(testString1))
print(separator * 50)

# Concatenate
testString3 = testString1 + testString2
print(testString3)
print(separator * 50)

# Or we can concatenate like this:
s = "New"
s += "York"
print(s)
print(separator * 50)

# Join
testString5 = '-'.join(['This', 'Should', 'Be', 'Jointed'])
print(testString5)
print(separator * 50)

# Format
testString4 = "The age of {0} is {1}".format('Jim', 32)
print(testString4)
print(separator * 50)

pos = (65.2, 23.1, 82.2)
testString5 = "Current position x={pos[0]} y={pos[1]} z={pos[2]}".format(
    pos=pos)
print(testString5)
print(separator * 50)


#Math format
testString6 = "Math constants: p={m.pi},e={m.e}".format(m=math)
testString7 = "Math constants: p={m.pi:.3f},e={m.e:.3f}".format(m=math)
print(testString6)
print(testString7)
print(separator * 50)
print(help(str))
