urls = {
    'Google': 'https://www.google.com',
    'YouTube': 'https://www.youtube.com/',
    'Facebook': 'https://www.facebook.com/',
    'Arduino': 'https://www.arduino.cc/',
    'Aliexpress': 'https://www.aliexpress.com/'
}

print(urls['Facebook'])

names_and_ages = [('Alice', 32), ('Bob', 22), ('Rob', 28), ('Andre', 26)]
d= dict(names_and_ages)
print(d)
print(urls)

another_list_of_names = [('Bogdan', 23), ('Mihai', 42), ('Maria', 28)]

print(another_list_of_names)
print("*********************")


dictionaryOfColors = dict(goldenrod=0xDAA520, indigo=0x4B0082,
                           seashell=0xFFF5EE)
print(dictionaryOfColors)
temporaryColors = dict(wheat=0xF5DEB3, khaki=0xF0e68C,
                       crimson=0xDC143C)
print(temporaryColors)

dictionaryOfColors.update(temporaryColors)
print(dictionaryOfColors)
print("A*********************")

for value in dictionaryOfColors.values():
    print(value)
print("B*********************")


for key in dictionaryOfColors.keys():
    print(key)
print("C*********************")


for key, value in dictionaryOfColors.items():
    print("{key} => {value}".format(key=key, value=value))

