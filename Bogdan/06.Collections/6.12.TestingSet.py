p = {6,28,5123,328973,183071807}
print(p)
print(type(p))

#set() constructor accept:
#   - iterable series of values
#   - duplicates are discarded
#   - often used specifically to remove duplicates
#   - not order preserving

e = {2, 2, 3, 4, 5, 6, 6, 6, 123, 123}
print(e)


for x in e:
    print(x)


q = {2, 9, 6, 4}
q.add(81)
print(q)
q.add(9)
print(q)


q.update([2, 4, 1023, 31321])
print(q)


#Set algebra (union, interception,
#Data set example

blue_eyes = {'Olivia', 'Harry', 'Lily', 'Jack', 'Amelia', }
blond_hair = {'Harry', 'Jack', 'Amelia', 'Mia', 'Joshua', }
smell_hcn = {'Harry', 'Amelia'}
taste_ptc = {'Harry', 'Lily', 'Amelia', 'Lola'}
o_blood = {'Mia', 'Joshua', 'Lily', 'Olivia'}
b_blood = {'Amelia', 'Jack'}
a_blood = {'Harry'}
ab_blood = {'Joshua', 'Lola'}

print(blue_eyes.union(blond_hair))
print(blue_eyes.union(blond_hair) == blond_hair.union(blue_eyes))
print(blue_eyes.intersection(blond_hair))
print(blond_hair.difference(blue_eyes))
print(blond_hair.symmetric_difference(blue_eyes))

print(smell_hcn.issubset(blond_hair))
print(taste_ptc.issuperset(smell_hcn))

print(a_blood.isdisjoint(o_blood))



