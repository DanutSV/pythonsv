w = "Let's search for something that is not here".split()
# i = w.index("duck")     #this should crash or do something stupid
# print(i)
import time
#Also, for searching in a list it is best to use "in" or "not in"

# print(3 in range(0,6))
if 3 in range(0,6):
    print("Yes, it is")

# print(4 not in range(5,10))
if 4 not in range(5,10):
    print("Not in range")


#To remove a value from a list you can use del in which you specify the
# position or remove and specify the value
#E.G
del w[6]
print(w)

w.remove('that')
w.remove('is')
w.remove('here')
print(w)


#Inserting a value in a string, or a word can be done by splitting the
# sentence, storing it into a list, inserting using insert which takes
# two arguments: an index and a value, and join the list to a sentence
# listToInsert.insert(index, "value")
#EG

temporaryList = "Today is: ".split()
temporaryList.insert(2, str(time.strftime("%d/%m/%Y")))   #strftime will return
# the date using %d/%m/%Y format and it's converted to a string using str

print(" ".join(temporaryList))              # to rejoin the words again,
# we need to apply the join to a string that will contain the separator we want
# in this case, beeing a space
# If for example we want as a separator to be an underscore, we will print
# something like
print("_".join(temporaryList))
print("*".join(w))
