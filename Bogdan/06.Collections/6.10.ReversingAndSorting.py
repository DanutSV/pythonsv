listForSort = [7, 5, 3, 9, 1, 8, 6, 2, 4]
print("The list looks like this:     "+str(listForSort))
listForSort.sort()
print("After has been sorted:        "+str(listForSort))
listForSort.reverse()
print("After reverse:                "+str(listForSort))


#Both operations before, can be done using the sort but with the argument
# reverse = True
anotherList = [22, 16, 17, 3, 1, 2, 6, 5]
anotherList.sort(reverse=True)
print("After sort with reverse=True: "+str(anotherList))


#Reversing and sorting can also be done on strings
#Splitting the string into a list, then, aplly .sort()
#with key=len as argument
#E.G.

justASentence = "I am not really sure if you documents are in the right " \
                "place".split()

# print(justASentence)
# justASentence.split()
justASentence.sort(key = len)
print(" ".join(justASentence))


