# Tuple - def : heterogeneous immutable sequence
char = '+'


print(char * 30)
t = ("Norway", 4.953, 3)
print(t[0])  # Acces tuple data by index 0
print(t[2])  # Acces tuple data by index 2
print(char * 30)


# To determine the length of the tuple we can use the bulit in len() function
print(len(t))
print(char * 30)


# To iterate the tuple
for item in t:
    print(item)
print(char * 30)


# We can concatenate using the '+' operator
print(t + (3201931, 234.0055))
print(char * 30)

# To repeat
print(t * 3)
print(char * 30)


# Tuples can contain any object thus, we can do things like this
# Also known as nested tuples
a = ((13, 16), (14, 17), (15, 18))
print(a[1])
print(a[2][1])
print(char * 30)


print(type(a))
print(char * 30)


# we can declare a tuple without parenthesis
p = 1, 1, 2, 3, 4, 1, 1, 1
print(p)
print(type(p))


#Example
def minmax(items):
    return min(items), max(items)
print(minmax(p))
