shopping_list = ["banana", "orange", "apple"] # Lista cu shopping stuff

stock = { # definirea stocurilor din magazin
    "banana": 6,
    "apple": 0,
    "orange": 32,
    "pear": 15
}

prices = { # preturi pentru stockuri
    "banana": 4,
    "apple": 2,
    "orange": 1.5,
    "pear": 3
}


# Write your code below!
def compute_bill(food): #definirea functiei in care se intampla stuff
    total = 0 # definirea totalului initial (0)
    for item in food:
        if stock[item] > 0: # parcurgerea stock-urilor care difera de 0
            total = total + prices[item] # calcularea totalului la cumpararea unui produs
            stock[item] = stock[item] - 1 # scoaterea unei unitati din stoc-ul respectiv
    return total


print total