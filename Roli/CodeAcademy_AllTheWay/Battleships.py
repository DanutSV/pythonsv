from random import randint

board = [] # printeaza o lista goala care deserveste drept tabla de joc

for x in range(0, 5):
  board.append(["O"] * 5) # genereaza un rand de zerouri

def print_board(board): # scoate virgulele din lista de zerouri
  for row in board:
    print " ".join(row)

print_board(board)


def random_row(board): #functie care genereaza randurile
  return randint(0, len(board) - 1)


def random_col(board): #functie care genereaza coloanele
  return randint(0, len(board[0]) - 1)


ship_row = random_row(board) # generarea pozitiei unei barci
ship_col = random_col(board) # generarea pozitiei unei barci
print ship_row # printarea barcilor for testing
print ship_col



for turn in range(4): # "for" pentru multiple incercari de a nimerii nava ( 4 in cazul nostru )
  print "Turn", turn + 1
  guess_row = int(raw_input("Guess Row: "))
  guess_col = int(raw_input("Guess Col: "))

  if guess_row == ship_row and guess_col == ship_col: # "for" care verifica daca ai nimerit barca cu conditiile aferente
    print "Congratulations! You sank my battleship!"
    break
  else:
    if guess_row not in range(5) or \
      guess_col not in range(5):
      print "Oops, that's not even in the ocean."
    elif board[guess_row][guess_col] == "X":
      print( "You guessed that one already." )
    else:
      print "You missed my battleship!"
      board[guess_row][guess_col] = "X"
    print_board(board)
if turn == 3: # "if" care termina jocul in momentul in care ti-ai depasit numarul de ture admise
  print "Game Over"
else:
  print "You missed for %s times" % turn # afiseaza de cate incercari ai avut nevoie inainte sa nimeresti barca