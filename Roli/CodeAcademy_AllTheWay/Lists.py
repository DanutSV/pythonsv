inventory = { # definire dictionar
  'gold' : 500,
  'pouch' : ['flint', 'twine', 'gemstone'], # Assigned a new list to 'pouch' key
  'backpack' : ['xylophone','dagger', 'bedroll','bread loaf']
}

# Adding a key 'burlap bag' and assigning a list to it
inventory['burlap bag'] = ['apple', 'small ruby', 'three-toed sloth']

# Sorting the list found under the key 'pouch'
inventory['pouch'].sort()

# Adaugare la lista pocket (seashell, strange berry ,lint )
inventory['pocket'] = ['seashell', 'strange berry', 'lint']
inventory['backpack'].sort()# sortare in backpack
inventory['backpack'].remove('dagger')# stergere dagger din lista backpack
inventory['gold'] = inventory['gold'] + 50 # valoare noua la key-ul gold
