import time
import random
import pandas as pd

print " ____    ____  _    _     ____    ____  _     ____   _ "
print "/  _ \  /_   \/ \  / \   /  _ \  /  __\/ \ /\/  _ \ / \ "
print "| / \|   /   /| |  | |   | / \|  |  \/|| | ||| | // | |"
print "| \_/|  /   /_| |  | |_/\| |-||  |  __/| \_/|| |_\\\ \_/"
print "\____/  \____/\_/  \____/\_/ \|  \_/   \____/\____/ (_)"
print "                                                     "

andrei_something_else = 'Andrei has something else to do'

meniu = [
    "Cola",
    "Water",
    "Wiskey",
    "Chicken",
    "Meat",
    "Mashed potatos",
    ]
df = pd.DataFrame(meniu, columns=["Meniu"])
df.loc[:2, "Prices"] = 5.50
df.loc[3, "Prices"] = 7.50
df.loc[4, "Prices"] = 6.50
df.loc[5:, "Prices"] = 7.50
df.index += 1

total_bill = 0.0

agenda = {'Bogdan': '+407 7666 52 34',
          'Alina': '+407 2346 43 46',
          'Andrei': '+407 8562 20 15',
          'Andreea': '+407 5842 23 90', }

listapr = []


def true(total_bill):

        order = raw_input(">>>  ")

        if order == 'done':
            print "Your total bill is ${:.2f}.".format(total_bill)
            print "Come again! *(Indian accent)*"
            exit()

        elif int(order) in df.index:
            item = df.loc[int(order), "Meniu"]     # Get the respective items
            price = df.loc[int(order), "Prices"]    # by indexing order input.
            print "You've selected {}! That would be ${:.2f}.".format(item, price)
            total_bill += price
            true(total_bill)

        elif order > 6:
            print("We don't have this product")
            print("Is there anything else you want from our menu?")
            true(total_bill)


def pmeniu():

    print df.to_string(justify='left',
                   header=False,
                   formatters={
                    'Meniu':'{{:<{}s}}'.format(
                        df['Meniu'].str.len().max()
                        ).format,
                    'Prices':'     ${:.2f}'.format})
    print("*Input the number associated to the product*")
    print("*Input 'done' when you got everything you need*")
    true(total_bill)


def intrare():
    print "Those friend will come with you:"
    print '\n'.join(listapr)
    print "As you reached the pub, a waitress is aproaching you "
    z = str(raw_input("*How can I help you? Do you want a normal place or a vip place*? (Type 'help' for help) "))
    if z == "normal":
        normala()
    if z == "vip":
        vip()
    if z == "help":
        print("normal place = Food menu + Drinks"'\n'"vip place = Something goes here")
        return intrare()
    else:
        print("normal place = Food menu + Drinks"'\n'"vip place = Something goes here")
        return intrare()


def normala():

    m = raw_input("*Please follow me*"'\n'"*Can I bring you a menu?*")
    if m == 'yes':
        pmeniu()

    elif m == "no":
        print("You have to order something or we'll have to get you out!")
        print("*Kicking the client out!*")
        exit()



def dimineata():

    print "You wake up in the morning , It's 11 o'clock and you don't have any idea what to do! \n So you thought about calling friends for a coffee"
    time.sleep(5)
    print "You get out of bed and thinking about brushing your teeth"
    dinti()



def dinti():

    d = raw_input("Do you want to brush your teeth?")
    if d == "yes":

        print "You are fresh! ( Girls would love to join for a coffee )"
        time.sleep(5)
        print "You are looking through your phone's contact:"
        for name in agenda:
            print (name, agenda[name])
        print ("Who do you want to call?")
        telefon(raw_input())

    if d == "no":

        print "Something smells! ( Girls wouldn't like to join for a coffee )"
        agenda.pop('Alina', None)
        agenda.pop('+407 2346 43 46', None)
        agenda.pop('Andreea', None)
        agenda.pop('+407 5842 23 90', None)
        print "You are looking through your phone's contact: *Girls are excluded because something's fishy!*"

        for name in agenda:
            print (name, agenda[name])
        print ("Who do you want to call?")
        telefon(raw_input())


def telefon(h):

    if h == "no":
        intrare()

    if h not in agenda:
        mesaj_eroare_persoana = "This person is not in our agenda!"
        print(mesaj_eroare_persoana)
        return telefon(raw_input())
    calling = "**Calling "
    separator = " **"
    mesaj_concatenat = calling + h + separator
    come = " comes with you"
    sansab = h + come
    not_come = " has something else to do"
    sansac = h + not_come
    print(mesaj_concatenat)
    time.sleep(5)
    chance = random.randint(0, 100)

    if chance <= 50:
        print (sansab)
        listapr.extend([h])


    elif chance >= 51:
        print (sansac)

    print ("Do you want to call someone else?")
    telefon(raw_input())


dimineata()


