#We have a class defined for vehicles. Create two new vehicles called car1 and car2.
#  Set car1 to be a red convertible worth $60,000.00 with a name of Fer,
# and car2 to be a blue van named Jump worth $10,000.00.

class Vehicle:
    name = ""
    kind = ""
    color = ""
    value = 100.00
    def description(self):
        desc_str = "%s is a %s %s worth $%.2f." % (self.name, self.color, self.kind, self.value)
        return desc_str



car1 = Vehicle()
car1.name = "Fer"
car1.kind = "Convertible"
car1.color = "Red"
car1.value = 6000000


car2 = Vehicle()
car2.name = "Jump"
car2.kind = "Van"
car2.color = "Blue"
car2.value = 1000000


print(car1.description())
print(car2.description())