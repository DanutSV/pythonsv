from random import randint   # guess the number game Mastermind


def difficulty(dif):
    if dif is 1:
        x = randint(10, 99)
    elif dif is 2:
        x = randint(100, 999)
    elif dif is 3:
        x = randint(1000, 9999)
    else:
        print("That difficulty does not exist")
        exit()
    return x


def main():
    x = 0
    inc = 0
    count = 1
    print("Please choose difficulty (1 to 3): ")
    dif = input()
    x = difficulty(int(dif))
    c = [int(z) for z in str(x)]  # prev generated number converted to a list
    while True:
        try:
            inc += 1
            n = input('{count}> '.format(count=count))
            count += 1

            if len(n) is not len(c):
                print('Please enter a {y} digit number! \n'.format(y=len(c)))
                continue
            _n = [int(z) for z in str(n)]
            star_count = 0  # number of correct digits

            for i in range(0, len(c)):
                if _n[i] == c[i]:
                    star_count += 1
            print("*" * star_count)

            if star_count == len(c):
                print('Correct! You did it in {inc} tries'.format(inc=inc))
                break
        except ValueError:
            print('Please enter a 4 digit number! \n')
            continue


if __name__ == "__main__":
    main()
