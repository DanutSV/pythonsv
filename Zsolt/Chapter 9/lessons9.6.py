class Flight:
    def __init__(self, number, aircraft):
        if not number[:2].isalpha:
            raise ValueError("No airline code in '{}' ".format(number))

        if not number[:2].isupper():
            raise ValueError("Invalid airline code in '{}'".format(number))

        if not (number[2:].isdigit() and int(number[2:]) <= 9999):
            raise ValueError("Invalid route number '{}'".format(number))

        self._number = number
        self._aircraft = aircraft

    def number(self):
        return self._number

    def airline(self):   # return airline name
        return self._number[:2]

    def aircraft_model(self):
        return self._aircraft.model()


class Aircraft:
    def __init__(self, registration, model, nr_rows, nr_seats_per_row):
        self._registration = registration
        self._model = model
        self._nr_rows = nr_rows
        self ._nr_seats_per_row = nr_seats_per_row

    def registration(self):
        return self._registration

    def model(self):
        return self._model

    def seating_plan(self):
        return (range(1, self._nr_rows + 1),
                "ABCDEFGHJK"[:self._nr_seats_per_row])


f = Flight("BA758", Aircraft("G-EUPT", "AIRBUS A319", 22, 6))
print(f.aircraft_model())

