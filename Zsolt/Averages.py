# def read_from_file():
#     values = open('values', mode='r', encoding='utf-8')
#     with open('values', 'r', encoding='utf-8') as infile:
#         lista = []
#         for line in infile:
#             lista.append(float(line))
#     values.close()
#     return lista


def builder():
     """
     user entered list
     :return: list
     """
     n = int(input('Number of values: \n'))
     lista = []
     for i in range(0, n):
         lista.append(float(input("Value {i} is ".format(i=i+1))))
     return lista


def average(lista):
    suma = 0
    for i in range(0, len(lista)):
        suma = suma + lista[i]
    avg = suma / len(lista)
    return avg


if __name__ == '__main__':
#    lista = read_from_file()
    lista = builder()
    print("{0:.2f}".format(average(lista)))    # print with 2 decimals


