def create_bingo(bingo_list):  # create a list with bingo values
    print("Welcome to bingo! \n"
          "Here is your board! \n")
    for i in range(1, 101, 1):
        bingo_list.append(i)
    return bingo_list


def print_table(print_list):   # go through the list and print it 10x10
    for i in range(len(print_list)):
        if print_list[i] % 10 == 0:
            print("_", end=" ")
            print()
        else:
            print("_", end=" ")  # write in one line dynamically


def print_bingo(have_bingo):  # print the list if it has bingo
    new_line = 0
    for i in range(len(have_bingo)):
        if have_bingo[i] is 0:
            print("__", end=" ")
        elif have_bingo[i] < 10:
            print('', have_bingo[i], end=" ")
        else:
            print(have_bingo[i], end=" ")
        if new_line is 9:
            print()
            new_line = -1
        new_line += 1


def request_numbers(user_list, bingo_list):  # main function
    b = False
    while True:
        try:
            n = int(input("Please enter your numbers: "))
            if n > 100 or n < 1:
                print('Please enter a number between 1 and 100')
                continue
        except ValueError:
            print('Wrong number')
        user_list[n - 1] = int(n)
        print(user_list)
        inc = 0
        for i in range(1, 91, 10):
            for j in range(i, i+10):
                if int(user_list[j-1]) == int(bingo_list[j-1]):
                    inc += 1
            if inc == 10:
                print("BINGO")
                b = True
                print_bingo(user_list)
            inc = 0
        if b is True:
            break


def empty_lists(empty_list):  # create an empty list with 100 elements all 0
    for i in range(0, 100):
        empty_list.append(0)
    return empty_list


board = []
user = []
user = empty_lists(user)
if __name__ == "__main__":
    board = create_bingo(board)
    print_table(board)
    request_numbers(user, board)
