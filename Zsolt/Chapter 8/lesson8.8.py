def gen123():
    print('About to print 1')
    yield 1
    print('About to print 2')
    yield 2
    print('About to print 3')
    yield 3


g = gen123()


print(next(g))

print(next(g))

print(next(g), '\n')  # sa las un rand liber


for v in gen123():
    print(v)
