from pprint import pprint as pp


million_squares = (x*x for x in range(1, 1000001))  # it is just a generator object

print(million_squares)

# pp(list(million_squares))

print(sum(x*x for x in range(1, 1000001)))

