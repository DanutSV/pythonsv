def take(count, iterable):  # take items from the front of a list
    counter = 0
    for item in iterable:
        if counter == count:
            return
        counter += 1
        yield item


def run_take():  # gives a list and prints the items from take
    items = [2, 4, 6, 8, 10]
    for item in take(5, items):
        print(item)


if __name__ == '__main__':
    run_take()

print('\n')  # makes an empty line space


def distinct(iterable):  # removes duplicates
    seen = set()
    for item in iterable:
        if item in seen:
            continue  # finishes the current loop and starts the next one
        yield item
        seen.add(item)


def run_distinct():  # prints the list without duplicates
    items = [5, 7, 7, 6, 5, 5]
    for item in distinct(items):
        print(item)


if __name__ == '__main__':
    run_distinct()

print('\n')


def run_pipeline():
    items = [3, 6, 6, 2, 1, 1]
    for item in take(3, distinct(items)):  # prints the first 3 distinct items
        print(item)


if __name__ == '__main__':
    run_pipeline()