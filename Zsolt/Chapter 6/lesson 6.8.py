w = "the quick brown fox jumps over the lazy dog".split()
print(w)

print(w.index("fox"))


print(w.count("the"))
del w[2]

print(w)

w.remove('lazy')
print(w)

w.insert(2, 'brown')
print(w)

s = ' '.join(w)
print(s)
