names_and_ages = [ ('Alice' , 22), ('Marian', 29), (' Gyuri', 44)]
d = dict(names_and_ages)
print(d)


c = dict(a='alpha', b='beta', o='omega')
print(c)

e = d.copy()
print(e)

f = dict(c)
print(f)

for key,value in f.items():
    print('{keys} => {value}'.format(keys=key, value=value))

m = {
    'H': [1, 2, 3],
    'He': [3, 4],
    'Li': [6, 7],
    'Be': [7, 9, 10] }

m['H'] += [4, 5, 6, 7]
print(m)
m['N'] = [13, 14, 15]
print(m)




