s = 'show how to index into sequences'.split()

print(s[4])
print(s[-1])
print(s[1:4]) #slice from a list
print(s[1:-1]) #slice after first till before last
print(s[2:]) #slice from pos2
print(s[:3]) #slice until pos3

