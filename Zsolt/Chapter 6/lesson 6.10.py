g = [1, 11, 21, 1211, 112111]
g.reverse()

print(g)

g.sort()
print(g)

g.sort(reverse=True)
print(g)

s = 'string full of words'.split()
print(s)

s.sort(key=len)

print(s)
