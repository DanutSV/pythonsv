def create_bingo(bingo_list):
    # create a list with bingo values
    print("Welcome to bingo! \n"
          "Here is your board! \n")
    for i in range(1, 101, 1):
        bingo_list.append(i)
    return bingo_list


def print_table(print_list):
    # go through the list and print it as an unknown bingo table
    for i in range(len(print_list)):
        if print_list[i] % 10 == 0:
            print("_", end=" ")
            print()
        else:
            print("_", end=" ")  # write in one line


def request_numbers(user_list, bingo_list):
    ajutor = 0
    b = False
    while True:
        n = input("Please enter your numbers: ")
        user_list[ajutor] = int(n)
        ajutor += 1
        ls = user_list[:ajutor]
        ls = remove_duplicates(ls)
        if has_duplicate is True:
            ajutor -= 1
            user_list = list(ls) + user_list[ajutor:]
        else:
            user_list = list(ls) + user_list[ajutor:]
#        print(user_list)
        inc = 0
        for i in range(1, 91, 10):
            for j in range(i, i+10):
                if int(user_list[j-1]) == int(bingo_list[j-1]):
                    inc += 1
            if inc == 10:
                print("BINGO")
                b = True
            inc = 0
        if b is True:
            break


def remove_duplicates(cut_list):
    global has_duplicate
    has_duplicate = False
    new_list = set(cut_list)
    if len(new_list) != len(cut_list):
        has_duplicate = True
    return new_list


def empty_lists(empty_list):  # create an empty list with 100 elements all 0
    for i in range(0, 100):
        empty_list.append(0)
    return empty_list


board = []
user = []
user = empty_lists(user)
if __name__ == "__main__":
    create_bingo(board)
    print_table(board)
    request_numbers(user, board)
