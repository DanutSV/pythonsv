import sys


def convert(s):
    '''Convert to an integer.'''
    try:
        return int(s)
    except (ValueError, TypeError) as e:
        print("Conversion error: {}"\
              .format(str(e)),
              file=sys.stderr)
        raise

print(convert(33))
print(convert('rau'))
print(convert([1, 2, 3]))

