def convert(s):
    '''Convert to an integer.'''
    try:
        x = int(s)
    except (ValueError, TypeError):
        x = -1
    return x

print(convert(33))
print(convert('rau'))
print(convert([1, 2, 3]))

