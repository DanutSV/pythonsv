import datetime
import time
time.ctime()


def show_default():
    print('\n', datetime.datetime.now(), '\n')



def banner(message, border=' '):
    line = border * len(message)
    print(line)
    print(message)
    print(line)


show_default()

banner('By Radu Iman', '*')