from employee import Employee

emp1 = Employee("Bogdan", 1232341)
emp2 = Employee("Roli", 112313)

emp1.displayEmployee()
emp2.displayEmployee()
print("Total Employee %d" % Employee.empCount)

emp1.age = 23
emp2.age = 21

emp1.displayEmployee()
print(emp1.age)

emp1.age = 25
emp1.displayEmployee()
print(emp1.age)


