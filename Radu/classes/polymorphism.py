class Animal:
    def __init__(self, name):
        self.name = name
    def talk(self):
        raise NotImplementedError("Subclass must implement abstarct method")

class Cat(Animal):
        def talk(self):
            return 'Miauu!'

class Dog(Animal):
    def talk(self):
        return 'Ham ham!'


animals = [Cat('Miti'),
           Cat('Miti2'),
           Dog('Blue')]

for animal in animals:
    print(animal.name + ' ' + animal.talk())
