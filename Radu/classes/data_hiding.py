class Counter:
    __secretCounter = 0

    def count(self):
        self.__secretCounter += 1
        print(self.__secretCounter)


counter = Counter()
counter.count()
counter.count()
print(counter._Counter__secretCounter)
