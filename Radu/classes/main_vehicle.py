from vehicle import Vehicle

car1 = Vehicle()
car2 = Vehicle()

car1.name = 'Fer'
car1.value = 60000.00
car1.color = "red"

car2.name = "Jump"
car2.value = 10000.00
car2.color = "blue"

print(car1.description())
print(car2.description())
