fo = open("foo.txt", 'wb')
print("Name of the file: ", fo.name)
print("Closed or not: ", fo.closed)
print("Opening mode: ", fo.mode)
# print("Softspace flag: ", fo.softspace)

fo.close()

# write()
print("Closed or not: ", fo.closed)
fo = open("foo.txt", 'w')
fo.write("Python is a great language.\n It is great.\n")
fo.close()

# read()
fo = open("foo.txt", 'r+')
str = fo.read(11)
print("Read string is: ", str)
# fo.close()

# fo = open("foo.txt", 'r+')
position = fo.tell()
print("Current file position: ", position)

position = fo.seek(35)
str = fo.read(16)
print("Again read string is: ", str)
fo.close()

import os

# os.rename("foo.txt", "test2.txt")
# os.remove("test2.txt")

# os.mkdir("test")
# os.chdir("test")
print(os.getcwd())
# os.chdir("..")
print(os.getcwd())
# os.rmdir("test")

fo = open("foo.txt", 'at')
fo.writelines(
    ['Son of man,\n',
     'You cannot say, or guess.'])

