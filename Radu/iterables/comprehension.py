# list comprehension

words = "Why sometimes I have believed as many as six impossible things before breakfast".split()
print(words, '\n')
print([len(word) for word in words])

lengths = []
for word in words:
    lengths.append(len(word))


print(lengths)


from math import factorial


f = [len(str(factorial(x))) for x in range(20)]
print(f)

print(type(f), '\n')

# set comprehension

f = {len(str(factorial(x))) for x in range(20)}
print(f)
print(type(f), '\n')

# dictionary comprehenfrom

from pprint import pprint

country_to_capital = {'Unit Kingdom': 'London',
                      'Brazil': 'Brasilia',
                      'Morocco': 'Rabat',
                      'Sweden': 'Stockholm'}
pprint(country_to_capital)
print('\n')

capital_to_country = {capital: country for country, capital in country_to_capital.items()}
pprint(capital_to_country)
print('\n')


words = ['hi', 'hello', 'foxtrot', 'hotel', 'firefox', 'google']
print({x[0]: x for x in words})
