# Reversing and Sorting Lists

lista = [1, 65, 74, 354, 54, 244, 7566, -35, -6, 0] #lista
print(lista)

lista.reverse()    #lista invers
print(lista)

lista.sort()       #crescator
print(lista)

lista.sort(reverse = True)   #descrescator
print(lista)


lista = 'not perplexing do handwriting family where I illegibily know doctors'.split() #ordoneaza dupa len de cuv
print(lista)
lista.sort(key=len)
print(' '.join(lista))


A = [1, 6, 3]
B = sorted(A)   #ordoneaza crescator
print(B)

C = [1, 6, 3]
D = reversed(C)  #invers
print(list(D))
