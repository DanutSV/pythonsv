p = {1, 43, 64, 243, 3423, 52352}
print(p)
print(type(p))
d = {}
print(type(d))
s = set()
print(type(s))
s = set([1, 54, 654, 3, 6, 34, 123])
print(s)
t = [1, 4, 2, 1, 7, 9, 9]
print(set(t))
for x in {1, 2, 4, 8, 16, 32}:
    print(x)
q = {2, 9, 4, 6}
print(8 in q)
print(8 not in q)
k = {81, 108}
k.add(54)
print(k)
k.add(12)
print(k)
k.add(12)
print(k)
k.update([37, 125, 544543])
print(k)
k.remove(12)
print(k)
k.remove(544543)
print(k)
# k.remove(123)
# print(k)
k.discard(123)
print(k)
j = k.copy()
print(j)
y = set(j)
print(y)

