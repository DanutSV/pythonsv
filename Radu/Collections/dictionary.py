from pprint import pprint

urls = {'Google': 'http://google.com',
         'Pluralsight': 'http://pluralsight.com',
         'Sixty North': 'http://sixty-north.com',
         'Microsoft': 'http://microsoft.com'}

print(urls['Google'])

names_and_ages = [('Alin', 32), ('Mihai', 19), ('Robert', 14), ('Andrei', 45)]
print(names_and_ages)
d = dict(names_and_ages)
print(d)
m = dict(cola=5, fanta=6, apa=3)
print(m)
n = m.copy()
print(n)

colors = dict(aqumarine='#7FFFD4', burlywood='#DEB887',
              chartreuse='#7FFF00', cornflower='#6495ED',
              firebrick='#B22222', honeydew='#F0FFF0',
              maroon='#B03060', sienna='#A0522D')
for key in colors:
    print("\n{key} => {value}".format(key=key, value=colors[key]))


for value in colors.values():
    print(value)


for key, value in colors.items():
    print("{key} => {value}".format(key=key, value=value))

m = {'H': [1, 2, 3],
     'He': [3, 4],
     'Li': [6, 7],
     'Be': [7, 9, 10],
     'B': [10, 11],
     'C': [11, 12, 13, 14]}
print(m)
m['H'] += [13, 14, 15]
print(m)
pprint(m)