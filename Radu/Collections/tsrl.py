from Radu.objects.banner_function import *


banner("Tuple, String, Range, List", "*")


s = "show how to index into sequences".split()
#      0   1   2   3     4      5
#     -6  -5  -4  -3    -2     -1

print(s)
print(s[4])
print(s[-4])
print(s[1:4])
print(s[1:-1])
print(s[3:])
print(s[:3])
print(s[:3] + s[3:] ==s)
print(s[0:] == s[:6])
full_slice = s[:]
print(full_slice)
print(full_slice is s)
print(full_slice == s)
print(id(s))
print(id(full_slice))
t = s[:]      #full slice
m = s.copy()  #copy method
c = list(s)   #constructor
print("full", t)
print("method", m)
print("constructor", c)