m = {1, 1, 2, 432, 1323}
l = ['Radu', 'Radu', 'Mihai', 'Jack']
print('\n', m)
print(l)


print('\nContainer: in AND not in. Ex. Radu in/(not in) l:')
print('Radu' in l)
print('Radu' not in l)


print('\nSized: len(). Ex. len(m) si len(l): ')
print(len(m))
print(len(l))


print('\nIterable: iter(), for item in iterable: do_something(item)')
print(iter(m), '     iter(m)')
iterable = iter(m)
for item in iterable:
    print(item)


print('\nSequence: item = seq[index], index = seq.index(item), num = seq.count(item), r = reversed(seq)')
index = l.index('Radu')
print(index)
item = l[index]
print(item)
num = l.count('Radu')
print(num)
r = reversed(l)
print(r)
print(l)

