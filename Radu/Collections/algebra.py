blue_eyes = {'Olivia', 'Harry', 'Lily', 'Jack', 'Amelia'}
blond_hair = {'Harry', 'Jack', 'Amelia', 'Mia', 'Joshua'}
smell_hcn = {'Harry', 'Amelia'}
taste_ptc = {'Harry', 'Lily', 'Amelia', 'Lola'}
o_blood = {'Mia', 'Joshua', 'Lily', 'Olivia'}
b_blood = {'Amelia', 'Jack'}
a_blood = {'Harry'}
ab_blood = {'Joshua', 'Lola'}

print(blue_eyes.union(blond_hair), '--->blue_eyes unit cu blond_hair')
print(blue_eyes.union(blond_hair) == blond_hair.union(blue_eyes), '-' * 57, '--->comutativa')
print(blue_eyes.intersection(blond_hair), '-' * 34, '--->blue_eyes intersectat cu blond_hair')
print(blue_eyes.intersection(blond_hair) == blond_hair.intersection(blue_eyes), '-' * 57, '--->comutativa')
print(blond_hair.difference(blue_eyes), '-' * 44, '--->blond_hair minus blue_eyes')
print(blond_hair.difference(blue_eyes) == blue_eyes.difference(blond_hair), '-' * 56, '--->necomutativa')
print(blue_eyes.difference(blond_hair), '-' * 43, '--->blue_eyes minus blond_hair')
print(blond_hair.symmetric_difference(blue_eyes), '-' * 26, '--->blue_ayes unit cu blond_hair minus blue_ayes intersectat cu blond_hair')
print(blond_hair.symmetric_difference(blue_eyes) == blue_eyes.symmetric_difference(blond_hair), '-' * 57, '--->comutativa')
print(smell_hcn.issubset(blond_hair), '-' * 57, '--->smell_hcn subset al blond_hair')
print(taste_ptc.issuperset(smell_hcn), '-' * 57, '--->taste_ptc superset al smell_hcn')
print(a_blood.isdisjoint(o_blood), '-' * 57, '--->disjoint')





