a = [ [1, 2], [3, 4]]
b = a[:]
print(a is b)
print(a == b)
print(a[0])
print(b[0])
print(b[1])
print(a[0] is b[0])
a[0] = [5, 6]
print(a[:])
a[1].append(5)
print('A: ', a[:], '\nB: ', b[:])

# list repetition

list = [[1, 2]] * 5
print(list)
list[1].append(3)
print(list)

w = "the quick brown fox jumps over the lazy dog".split()
print(w)
i = w.index('fox')
print(i)
print(w[i])
print(w.count('the'))
print('lazy' in w)
print('jumps' not in w)
del w[1]
w.remove('dog')
print(w)
w.insert(1, 'lazy')
w.insert(8, 'cat')
print(w)
print(' '.join(w))
print(w + a)



