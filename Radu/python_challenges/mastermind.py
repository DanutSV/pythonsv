import random

print("\nWelcome, I'm Mastermind!\n")

print("1. Play Game")
print("2. High Score")

first_option = input('Select Option. \n')
if first_option == '1':
    score = 0
    chance = 5
    numbers = []
    name = input('Enter your name. \n')
    print("1. Beginner")
    print("2. Professional")
    print("3. Legendary")
    second_option = input("Select a difficulty! \n")
    if second_option == '1':
        while chance > 0:
            number = int(input("Enter a number between 0 and 10. "))
            rand = random.randint(0, 10)
            if number == rand:
                score += 1
            numbers.append(rand)
            chance -= 1
    elif second_option == '2':
        while chance > 0:
            number = int(input("Enter a number between 0 and 25. "))
            rand = random.randint(0, 25)
            if number == rand:
                score += 1
            numbers.append(rand)
            chance -= 1
    elif second_option == '3':
        while chance > 0:
            number = int(input("Enter a number between 0 and 50. "))
            rand = random.randint(0, 50)
            if number == rand:
                score += 1
            numbers.append(rand)
            chance -= 1
    f = open("highscore", "r+")

    high_score = f.read()
    index = high_score.index('" ') + 1
    print(index)
    print(f.read(index))

    game = '\n' + "\"" + name + "\"" + ' ' + str(score) + ' Random numbers: ' + str(numbers)
    print(game)
    f.write(game)
elif first_option == '2':
    f = open("highscore", 'r+')
    print(f.read())
