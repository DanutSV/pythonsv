n1 = 1
n2 = 1
elem = int(input("Introduceti numarul de elemente  ")) - 2
print(n1, '\n', n2)
while elem > 0:
    next = n1 + n2
    print(next)
    elem -= 1
    n1 = n2
    n2 = next

# c = 'A'
# print("ASCII code ", ord(c))

# Python program to display fibonacci sequence using recursion

def recur_fibonacci(n):
   if n <= 1:
       return n
   else:
       return recur_fibonacci(n - 1) + recur_fibonacci(n - 2)


nrterms = int(input("Enter a positive integer: "))

if nrterms <= 0:
    print('Please enter a positive integer!')
else:
    print("Fibonacci sequence")
    for i in range(nrterms):
        print(recur_fibonacci(i))

