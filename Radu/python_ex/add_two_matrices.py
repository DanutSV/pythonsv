A = [[4, 5, 5],
     [3, 5, 1],
     [9, 4, 3]]

B = [[3, 5, 7],
     [4, 7, 3],
     [5, 8, 2]]

r = [[0, 0, 0],
     [0, 0, 0],
     [0, 0, 0]]

for i in range(len(A)):
    for j in range(len(A[0])):
        r[i][j] = A[i][j] + B[i][j]

print('\nSum')
for r in r:
    print(r)


# transpose a matrix

X = [[1, 2],
     [3, 4],
     [5, 6]]

result = [[0, 0, 0],
          [0, 0, 0]]

i = j = 0

for i in range(len(X)):
    for j in range(len(X[0])):
        result[j][i] = X[i][j]
print('\nTranspose')
for result in result:
    print(result)


# multiply two matrices

M = [[12, 7, 3],
     [4, 5, 6],
     [7, 8, 9]]

N = [[5, 8, 1, 2],
     [6, 7, 3, 0],
     [4, 5, 9, 1]]

res = [[0, 0, 0, 0],
     [0, 0, 0, 0],
     [0, 0, 0, 0]]

i = j = 0
for i in range(len(M)):
    for j in range(len(N[0])):
        for k in range(len(N)):
            res[i][j] += M[i][k] * N[k][j]
print('\nMultiply')
for res in res:
    print(res)





