def multi(x, y):                      # patratul
    return x * y


def suma(x, y):                     # suma

    return x + y


def dif(x, y):                      # diferenta
    return x - y


def div(x, y):                      # impartit
    return x / y


""" 
    if you want to use this functions
    you need to import them in your main function
    Ex. from Radu.Modular.function import *
 """
