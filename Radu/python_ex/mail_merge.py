# names are in the file name.txt
# body of the mail is in body.txt


# open name.txt for reding
with open("name.txt", 'r', encoding='utf-8') as name_file:

    # open body.txt for reding
    with open("body.txt", 'r', encoding='utf-8') as body_file:

        # read content of the body
        body = body_file.read()

        # names from name.txt file
        for name in name_file:
            mail = 'Hello '+name+body

            # write the mails to individual files
            with open(name.strip()+".txt", 'w', encoding='utf-8') as mail_file:
                 mail_file.write(mail)