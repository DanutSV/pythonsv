E = {'Roli', 'Zsolt', 'Radu', 'Cosmin', 'George'};
N = {'Danut', 'Bogdan', 'Robert', 'Roli', 'Radu'};

# set union
print("\nUnion",E | N)

# set intersection
print("\nIntersection",E & N)

# set difference
print("\nDifference",E - N)

# set symmetric difference
print("\nSymmetric",E ^ N)