punctuations = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''
my_str = "ffhjh!!!, hejglk guihi .!!??gjh ./,][."

no_punct = ''

for char in my_str:
    if char not in punctuations:
        no_punct += char


print(no_punct)