import cmath


# a = float(input('a = '))
# b = float(input('b = '))
# c = float(input('c = '))

import random

a = random.randint(1, 9)
b = random.randint(0, 9)
c = random.randint(0, 9)

print("{}x^2 + {}x + {} = ".format(a, b, c))

d = (b ** 2) - (4 * a * c)

s1 = (-b + cmath.sqrt(d)) / (2 * a)
s2 = (-b - cmath.sqrt(d)) / (2 * a)

print("The solution are {0} and {1}".format(s1, s2))