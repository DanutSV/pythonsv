from math import sqrt


a = float(input('Enter First side : '))
b = float(input('Enter Second side: '))
c = float(input('enter Third side : '))

s = (a + b + c) / 2

area = (s * (s - a) * (s - b) * (s - c)) ** 0.5
print("The area of the triangle is: ", area)

# or

area = sqrt((s * (s - a) * (s - b) * (s - c)))
print("The area of the triangle is: ", area)