vowels = 'aeiou'
str = "Hello, give me the number of vowels"
str = str.casefold()

count = {}.fromkeys(vowels, 0)

for char in str:
    if char in count:
        count[char] += 1

print(count)