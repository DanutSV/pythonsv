
x = 3
y = 5
z = x + y
print('x + y = ', z)

for i in range(5):
    a = i * 10
    print('\n', i, '* 10 =', a)

import math

print('\nsqrt(9) = ', math.sqrt(9))
print('\nfactorial(5) = ', math.factorial(5))


print('\n y! / (x! * (y - x)) = ', math.factorial(y) / (math.factorial(x) * math.factorial(y - x)))

from math import factorial
print('\n  y! / (x! * (y - x)) = ', factorial(y) / (factorial(x) * factorial(y - x)))

print('\n', len(str(factorial(z))))
print('\n', len(str("Number of characters.")))

print('\n', int(3.5))
print('\n', int(-3.5))
print('\n', int("123"))
print('\n', int("10000", 3))
print('\n', float(1))
print('\n', float(-1))
print('\n', float("123"))
print('\n', float("1000"))
print('\n', bool(3))
print('\n', bool(-4))
print('\n', bool(0))
print('\n', bool([]))
print('\n', bool([1, 3, 6]))
print('\n', bool(""))
print('\n', bool("none"))

